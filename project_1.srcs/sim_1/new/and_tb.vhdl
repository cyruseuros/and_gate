library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity and_gate_tb is
end;

architecture bench of and_gate_tb is

  component and_gate
      Port ( in1, in2 : in STD_LOGIC;
             out1 : out STD_LOGIC);
  end component;

  signal in1, in2: STD_LOGIC;
  signal out1: STD_LOGIC;

begin

  uut: and_gate port map ( in1  => in1,
                           in2  => in2,
                           out1 => out1 );

  stimulus: process
  begin
  
    -- Put initialisation code here
    in1 <= '0';
    in2 <= '0';
    wait for 10 ns;
    
    in1 <= '0';
    in2 <= '1';
    wait for 10 ns;
    
    in1 <= '1';
    in2 <= '1';
    wait for 10 ns;
    
    in1 <= '1';
    in2 <= '0';
    wait for 10 ns;

    -- Put test bench stimulus code here

    wait;
  end process;


end;